from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

import getpass
import sys
import os
import time

from os import system, name 


# define our clear function 
def clear(): 
    os.system('color 1f') # sets the background to blue
    # for windows 
    if name == 'nt': 
        _ = system('cls') 
    # for mac and linux(here, os.name is 'posix') 
    else: 
        _ = system('clear') 


def program_banner():
    # now = time.time()
    # twodays_ago = now - 60*60*24*2 # Number of seconds in two days
    # if fileCreation < twodays_ago:
    #     print "File is more than two days old"
    print('====================================================================')
    print('===================== RENTAL RESULTS AUTOMATOR =====================')
    print('====================================================================')
    print('= PLEASE DOWNLOAD THE CONTACTS & OWNER FEES FROM REPORTS           =')
    print('= Goto Reports >> Contact Details - Tenant                         =')
    print('= Goto Reports >> Owner Fees                                       =')
    print('= Export to Excel. Copy and Paste the report from downloads folder =')
    print('= Root folder of the App                                           =')
    print('====================================================================')
    print("= Contact Details.xlsx [Modified: %s]        =" % time.ctime(os.path.getmtime("Contact Details.xlsx")))
    print("= Owner Fees.xlsx      [Modified: %s]        =" % time.ctime(os.path.getmtime("Owner Fees.xlsx")))
    print('====================================================================')
    print('')


def extract_data():
    clear()
    program_banner()
    # print("Last modified: %s" % time.ctime(os.path.getmtime("Contact Details.xlsx")))
    # print("Created: %s" % time.ctime(os.path.getctime("Contact Details.xlsx")))
    print('************* PropertyMe Signin *************')
    global username
    global password

    username = input('Username: ').strip()
    password = getpass.getpass().strip()

    clear()
    program_banner()
    print('Choose your Option:')
    print('\t1 = Welcome Letter')
    print('\t2 = Vacate Letter')
    print('\t3 = Break Lease Agreement')
    selection = input('Enter your Option: ')

    if selection == '1':
        keyword = welcome_letter()
    elif selection == '2':
        keyword = vacate_letter()
    elif selection == '3':
        keyword = break_lease_letter()
    else:
        keyword = welcome_letter()
        
    return selection,username,keyword

def welcome_letter():
    clear()
    program_banner()
    print('You have selected WELCOME Letter.')
    keyword = input('Search Filter (e.g. 1/11 Wongara): ')

    print('\nStarting browser')
    user_name = os.getlogin().lower()
    if 'joefer' in user_name:
        driver = webdriver.Firefox(executable_path='resources/geckodriver.exe', firefox_binary=FirefoxBinary("C://Program Files//Mozilla Firefox//firefox.exe"))
    else:
        driver = webdriver.Firefox(executable_path='resources/geckodriver.exe')
    driver.implicitly_wait(30)

    driver.get('https://app.propertyme.com/')

    # Portal Signin
    print('Signing into portal')
    driver.find_element_by_id('UserName').send_keys(username)
    driver.find_element_by_id('Password').send_keys(password)
    driver.find_element_by_css_selector('#submitButton').click()

    try:
        wait = WebDriverWait(driver, 60)
        element = wait.until(ec.element_to_be_clickable((By.CSS_SELECTOR, "[ng-href='#/property/list']")))
    except TimeoutException:
        driver.quit()
        print('\nInvalid login details')
        input('Press enter to exit...')
        sys.exit()

    # Navigate
    print('Navigating to Property')
    element.click()

    driver.find_element_by_css_selector('.scrolling-grid-container .search-box').send_keys(keyword)
    driver.find_element_by_css_selector('.scrolling-grid-container .icon-search').click()

    try:
        driver.find_element_by_css_selector('tr td:nth-child(2) a').click()
    except NoSuchElementException:
        driver.quit()
        print('No property found for given keyword.')
        input('Press enter to exit...')
        sys.exit()

    # Extract data
    print('Extracting Data')
    address = driver.find_element_by_xpath("//div[@label='Address']/div[2]").text
    email = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Ownership']//div[@label='Email']/div[2]").text
    owner = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Ownership']//div[@label='Contact']/div[2]").text
    owner_arr = owner.split(',')

    if len(owner_arr) > 1:
        firstname = owner_arr[1].strip()
        lastname = owner_arr[0].strip()
    else:
        firstname = owner_arr[0]
        lastname = ''

    file = open('PropertyMe.csv', 'w')
    file.write(f'"{address}","{firstname}","{lastname}","{email}"')
    file.close()
    driver.quit()
    return keyword
    

def vacate_letter():

    #required fields: date today, name of tenant, moveout date, tenant email, first name
    clear()
    program_banner()
    print('You have selected VACATE Letter.')
    keyword = input('Search Filter (e.g. 1/11 Wongara): ')

    print('\nStarting browser')
    user_name = os.getlogin().lower()
    if 'joefer' in user_name:
        driver = webdriver.Firefox(executable_path='resources/geckodriver.exe', firefox_binary=FirefoxBinary("C://Program Files//Mozilla Firefox//firefox.exe"))
    else:
        driver = webdriver.Firefox(executable_path='resources/geckodriver.exe')
    driver.implicitly_wait(30)

    driver.get('https://app.propertyme.com/')

    # Portal Signin
    print('Signing into portal')
    driver.find_element_by_id('UserName').send_keys(username)
    driver.find_element_by_id('Password').send_keys(password)
    driver.find_element_by_css_selector('#submitButton').click()

    try:
        wait = WebDriverWait(driver, 60)
        element = wait.until(ec.element_to_be_clickable((By.CSS_SELECTOR, "[ng-href='#/property/list']")))
    except TimeoutException:
        driver.quit()
        print('\nInvalid login details')
        input('Press enter to exit...')
        sys.exit()

    # Navigate
    print('Navigating to Property')
    element.click()

    driver.find_element_by_css_selector('.scrolling-grid-container .search-box').send_keys(keyword)
    driver.find_element_by_css_selector('.scrolling-grid-container .icon-search').click()

    try:
        driver.find_element_by_css_selector('tr td:nth-child(2) a').click()
    except NoSuchElementException:
        driver.quit()
        print('No property found for given keyword.')
        input('Press enter to exit...')
        sys.exit()

    # Extract data
    print('Extracting Data')
    address = driver.find_element_by_xpath("//div[@label='Address']/div[2]").text
    # email = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Ownership']//div[@label='Email']/div[2]").text
    email = 'sample@sample.com'
    owner = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Ownership']//div[@label='Contact']/div[2]").text
    owner_arr = owner.split(',')

    if len(owner_arr) > 1:
        firstname = owner_arr[1].strip()
        lastname = owner_arr[0].strip()
    else:
        firstname = owner_arr[0]
        lastname = ''

    tenant = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Tenancy']//div[@label='Contact']/div[2]").text
    tenant_arr = tenant.split(',')
    tenants = tenant
    tenants = tenants.replace(";", " & ")

    tenant_moveout_date = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Tenancy']//div[@label='Moved out']/div[2]").text
    tenant_email = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Tenancy']//div[@label='Email']/div[2]").text
    rent = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Tenancy']//div[@label='Rent']/div[2]").text

    # lb_fee = rent * 0.1 # 0.1 for GST
    # lb_fee = lb_fee + rent
    lb_fee = ""
    # if '&' in tenant:
    #     driver.get('https://app.propertyme.com/#/property/list')
    #     tenants = tenant
    #     #tenants = tenant.split('&')
    #     # try:
    #     #     driver.find_element_by_xpath("//div[@ng-show='vm.dto.Tenancy']//div[@label='Contact']/div[2]/a").click()
    #     # except NoSuchElementException:
    #     #     driver.quit()
    #     #     print('Error clicking Contacts.')
    #     #     input('Press enter to exit...')
    #     #     sys.exit()
    # else:
    if len(tenant_arr) > 1:
        t_Firstname = tenant_arr[1].strip()
        t_Lastname = tenant_arr[0].strip()
    else:
        t_Firstname = tenant_arr[0]
        t_Lastname = ''

    adfee = ''
    file = open('PropertyMeVacate.csv', 'w')
    file.write(f'"{address}","{firstname}","{lastname}","{email}","{t_Firstname}","{t_Lastname}","{tenant_moveout_date}","{tenant_email}","{tenants}","{lb_fee}","{rent}","{adfee}"')
    file.close()
    driver.quit()
    return keyword

def break_lease_letter():
    clear()
    program_banner()
    print('You have selected BREAK LEASE Letter.')
    keyword = input('Search Filter (e.g. 1/11 Wongara): ')

    print('\nStarting browser')
    user_name = os.getlogin().lower()
    if 'joefer' in user_name:
        driver = webdriver.Firefox(executable_path='resources/geckodriver.exe', firefox_binary=FirefoxBinary("C://Program Files//Mozilla Firefox//firefox.exe"))
    else:
        driver = webdriver.Firefox(executable_path='resources/geckodriver.exe')
    driver.implicitly_wait(30)

    driver.get('https://app.propertyme.com/')

    # Portal Signin
    print('Signing into portal')
    driver.find_element_by_id('UserName').send_keys(username)
    driver.find_element_by_id('Password').send_keys(password)
    driver.find_element_by_css_selector('#submitButton').click()

    try:
        wait = WebDriverWait(driver, 60)
        element = wait.until(ec.element_to_be_clickable((By.CSS_SELECTOR, "[ng-href='#/property/list']")))
    except TimeoutException:
        driver.quit()
        print('\nInvalid login details')
        input('Press enter to exit...')
        sys.exit()

    # Navigate
    print('Navigating to Property')
    element.click()

    driver.find_element_by_css_selector('.scrolling-grid-container .search-box').send_keys(keyword)
    driver.find_element_by_css_selector('.scrolling-grid-container .icon-search').click()

    try:
        driver.find_element_by_css_selector('tr td:nth-child(2) a').click()
    except NoSuchElementException:
        driver.quit()
        print('No property found for given keyword.')
        input('Press enter to exit...')
        sys.exit()

    # Extract data
    print('Extracting Data')
    address = driver.find_element_by_xpath("//div[@label='Address']/div[2]").text
    email = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Ownership']//div[@label='Email']/div[2]").text
    owner = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Ownership']//div[@label='Contact']/div[2]").text
    owner_arr = owner.split(',')

    if len(owner_arr) > 1:
        firstname = owner_arr[1].strip()
        lastname = owner_arr[0].strip()
    else:
        firstname = owner_arr[0]
        lastname = ''

    tenant = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Tenancy']//div[@label='Contact']/div[2]").text
    tenant_arr = tenant.split(',')
    tenants = tenant

    # tenant_moveout_date = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Tenancy']//div[@label='Moved out']/div[2]").text
    tenant_moveout_date = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Tenancy']//div[@label='Agreement']/div[2]").text
    tenant_moveout_date = tenant_moveout_date.split('to')
    tenant_moveout_date = tenant_moveout_date[1].strip()
    tenant_email = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Tenancy']//div[@label='Email']/div[2]").text
    rent = driver.find_element_by_xpath("//div[@ng-show='vm.dto.Tenancy']//div[@label='Rent']/div[2]").text

    rent = float(rent.replace("$",""))
    lb_fee = (rent * 0.1) # 0.1 for GST
    lb_fee = lb_fee + rent
    # if '&' in tenant:
    #     driver.get('https://app.propertyme.com/#/property/list')
    #     tenants = tenant
    #     #tenants = tenant.split('&')
    #     # try:
    #     #     driver.find_element_by_xpath("//div[@ng-show='vm.dto.Tenancy']//div[@label='Contact']/div[2]/a").click()
    #     # except NoSuchElementException:
    #     #     driver.quit()
    #     #     print('Error clicking Contacts.')
    #     #     input('Press enter to exit...')
    #     #     sys.exit()
    # else:
    if len(tenant_arr) > 1:
        t_Firstname = tenant_arr[1].strip()
        t_Lastname = tenant_arr[0].strip()
    else:
        t_Firstname = tenant_arr[0]
        t_Lastname = ''

    # adfee = input('Enter Advertising Fee: ')
    adfee = ''
    file = open('PropertyMeBreakLease.csv', 'w')
    file.write(f'"{address}","{firstname}","{lastname}","{email}","{t_Firstname}","{t_Lastname}","{tenant_moveout_date}","{tenant_email}","{tenants}","{lb_fee}","{rent}","{adfee}"')
    file.close()
    driver.quit()
    return keyword


