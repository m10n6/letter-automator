import os, sys, re, winsound, time, csv

from data_extractor import extract_data

import win32com.client as win32
import comtypes.client
from docx import Document

from datetime import datetime
from configparser import ConfigParser
from xlrd import open_workbook

# defaultEmailRecipient = 'lauren@rentalresults.com.au'
# defaultEmailRecipient = 'mark.ramos@affordablestaff.com.au'
defaultEmailRecipient = ''
is_debug = 'on'

def suffix(d):
    return 'th' if 11<=d<=13 else {1:'st',2:'nd',3:'rd'}.get(d%10, 'th')


def custom_strftime(format, t):
    return t.strftime(format).replace('{S}', str(t.day) + suffix(t.day))


def covx_to_pdf(infile, outfile):
    word = comtypes.client.CreateObject('Word.Application')
    doc = word.Documents.Open(infile)
    doc.SaveAs(outfile, FileFormat=17)
    doc.Close()
    word.Quit()

def docx_replace_word(doc_obj, word , replace):
    for p in doc_obj.paragraphs:
        if word.search(p.text):
            inline = p.runs
            # Loop added to work with runs (strings with same style)
            for i in range(len(inline)):
                if word.search(inline[i].text):
                    text = word.sub(replace, inline[i].text)
                    inline[i].text = text
    for table in doc_obj.tables:
        for row in table.rows:
            for cell in row.cells:
                docx_replace_word(cell, word , replace)


def emailer(subject, recipient):
    word = win32.Dispatch("Word.Application")
    doc = word.Documents.Open(os.path.join(welcomeLetterEmailNewPath, welcomeLetterEmailNewFilename))
    doc.Content.Copy()
    doc.Close()
    os.remove(os.path.join(welcomeLetterEmailNewPath, welcomeLetterEmailNewFilename))
    outlook = win32.Dispatch("Outlook.Application")
    mail = outlook.CreateItem(0)
    mail.To = recipient
    mail.Subject = subject
    mail.HTMLBody = '' #Keep this line to get rid of the Signature
    word_editor = mail.GetInspector.WordEditor
    abc = word_editor.Range(Start=0, End=0)
    abc.Collapse(0)
    abc.InsertAfter('<---  TARGET RECIPIENT:   ' + client_email_address + '   --->\n\n')
    abc.Collapse(0)
    abc.Paste()
    mail.Attachments.Add(os.path.join(serviceGuaranteePdfPath, serviceGuaranteePdfFilename))
    mail.Attachments.Add(os.path.join(welcomeDocPdfPath, welcomeDocPdfFilename))
    mail.Send()
    print('Email has been sent to ' + defaultEmailRecipient)

def vacate_emailer(subject, recipient):
    # print (orig_address)
    # tenant_email = get_tenants_email_by_address(orig_address)
    tenant_email = get_tenants_email_by_address(keyword)
    # print(address_unit_and_streetnumber_orig)
    # print(tenant_email)
    list_email = ''
    for e in tenant_email:
        list_email += e + ","

    word = win32.Dispatch("Word.Application")
    doc = word.Documents.Open(os.path.join(vacateEmailNEWTemplatePath, vacateEmailNEWTemplateFilename))
    doc.Content.Copy()
    doc.Close()
    os.remove(os.path.join(vacateEmailNEWTemplatePath, vacateEmailNEWTemplateFilename))
    outlook = win32.Dispatch("Outlook.Application")
    mail = outlook.CreateItem(0)
    mail.To = recipient
    mail.Subject = subject
    mail.HTMLBody = '' #Keep this line to get rid of the Signature
    word_editor = mail.GetInspector.WordEditor
    abc = word_editor.Range(Start=0, End=0)
    abc.Collapse(0)
    # abc.InsertAfter('<---  TARGET RECIPIENT:   ' + tenant_email + '   --->\n\n')
    abc.InsertAfter('<---  TARGET RECIPIENT:   ' + list_email.rstrip(",") + '   --->\n\n')
    abc.Collapse(0)
    abc.Paste()
    mail.Attachments.Add(os.path.join(vacateLetterPDFPath, vacateLetterPDFFilename))
    mail.Attachments.Add(os.path.join(vacateFORM14aRTAPath, vacateFORM14aRTAFilename))
    mail.Attachments.Add(os.path.join(vacateRentalResultsChecklistPath, vacateRentalResultsChecklistFilename))
    mail.Attachments.Add(os.path.join(vacateRTAMovingOutBookletPath, vacateRTAMovingOutBookletFilename))
    mail.Send()
    print('Email has been sent to ' + defaultEmailRecipient)

def breaklease_emailer(subject, recipient):
    # tenant_email = get_tenants_email_by_address(orig_address)
    tenant_email = get_tenants_email_by_address(keyword)
    list_email = ''
    for e in tenant_email:
        list_email += e + ","

    word = win32.Dispatch("Word.Application")
    #doc = word.Documents.Open(os.path.join(vacateEmailNEWTemplatePath, vacateEmailNEWTemplateFilename))
    doc = word.Documents.Open(os.path.join(breakLeaseEmailNEWTemplatePath, breakLeaseEmailNEWTemplateFilename))
    doc.Content.Copy()
    doc.Close()
    #os.remove(os.path.join(vacateEmailNEWTemplatePath, vacateEmailNEWTemplateFilename))
    os.remove(os.path.join(breakLeaseEmailNEWTemplatePath, breakLeaseEmailNEWTemplateFilename))
    outlook = win32.Dispatch("Outlook.Application")
    mail = outlook.CreateItem(0)
    mail.To = recipient
    mail.Subject = subject
    mail.HTMLBody = '' #Keep this line to get rid of the Signature
    word_editor = mail.GetInspector.WordEditor
    abc = word_editor.Range(Start=0, End=0)
    abc.Collapse(0)
    abc.InsertAfter('<---  TARGET RECIPIENT:   ' + list_email.rstrip(",") + '   --->\n\n')
    abc.Collapse(0)
    abc.Paste()
    #mail.Attachments.Add(os.path.join(vacateLetterPDFPath, vacateLetterPDFFilename))
    mail.Attachments.Add(os.path.join(breakLeaseAgreementPDFPath, breakLeaseAgreementPDFFilename))
    #mail.Attachments.Add(os.path.join(vacateFORM14aRTAPath, vacateFORM14aRTAFilename))
    mail.Attachments.Add(os.path.join(breakLeaseFORM14aRTAPath, breakLeaseFORM14aRTAFilename))
    #mail.Attachments.Add(os.path.join(vacateRentalResultsChecklistPath, vacateRentalResultsChecklistFilename))
    mail.Attachments.Add(os.path.join(breakLeaseRentalResultsChecklistPath, breakLeaseRentalResultsChecklistFilename))
    #mail.Attachments.Add(os.path.join(vacateRTAMovingOutBookletPath, vacateRTAMovingOutBookletFilename))
    mail.Attachments.Add(os.path.join(breakLeaseRTAMovingOutBookletPath, breakLeaseRTAMovingOutBookletFilename))
    mail.Send()
    print('Email has been sent to ' + defaultEmailRecipient)


def outlook_is_running():
    import win32ui
    try:
        win32ui.FindWindow(None, "Microsoft Outlook")
        return True
    except win32ui.error:
        return False


def option_picker():
    print('Warning! Outlook will be forcefully closed.')
    winsound.PlaySound("SystemHand", winsound.SND_ALIAS)
    time.sleep(1)
    print('\nWould you like to continue? [y/n]')
    time.sleep(1)
    yes = {'yes','y', 'ye'}
    no = {'no','n'}
    choice = input().lower()
    if choice in no:
        print()
        print('Exiting...')
        time.sleep(1)
        sys.exit()
    elif choice in yes:
        os.system('TASKKILL /F /IM outlook.exe')
        pass
    else:
        print('Invalid input!')
        winsound.PlaySound("SystemExit", winsound.SND_ALIAS)
        time.sleep(1)
        print('\nThis app will now close.')
        time.sleep(1)
        sys.exit()

def get_tenants_email_by_address(search_str):
    search_str = search_str.replace(",","")
    book = open_workbook('Contact Details.xlsx')
    # book = open_workbook('Client Access.xlsx')
    v = []
    for sheet in book.sheets():
        for rowidx in range(sheet.nrows):
            row = sheet.row(rowidx)
            for colidx, cell in enumerate(row):
                # search_str = "1/25 Princess St\nBulimba QLD 4171"
                # print (cell.value)
                str_ = str(cell.value)
                newstr = str_.replace("\n", " ")
                # print (newstr)
                #if newstr == search_str :
                if search_str in newstr :
                    # print (sheet.name)
                    # print (colidx)
                    # print (rowidx)
                    # print (sheet.cell(rowidx, colidx-4).value)
                    # print (search_str)
                    x = str(sheet.cell(rowidx, colidx-4).value)
                    # x = str(sheet.cell(rowidx, colidx-3).value)
                    # print (x)
                    # print (newstr)
                    # print (search_str)
                    v.append(x)
                # else:
                #     v.append('hello')

                # print (newstr)
                # print (search_str)
                # return v
        return (v)    

def get_adsfee_by_address(search_str):
    search_str = search_str.replace(",","")
    book = open_workbook('Owner Fees.xlsx')
    # book = open_workbook('Client Access.xlsx')
    # v = []
    v = ''
    for sheet in book.sheets():
        for rowidx in range(sheet.nrows):
            row = sheet.row(rowidx)
            for colidx, cell in enumerate(row):
                # search_str = "1/25 Princess St\nBulimba QLD 4171"
                # print (cell.value)
                str_ = str(cell.value)
                newstr = str_.replace("\n", " ")
                # print (newstr)
                #if newstr == search_str :
                if search_str in newstr :
                    # print (sheet.name)
                    # print (colidx)
                    # print (rowidx)
                    # print (sheet.cell(rowidx, colidx-4).value)
                    # print (search_str)
                    adfee_text = str(sheet.cell(rowidx, colidx + 1).value)
                    if 'Advertising Fee' in adfee_text:
                        x = str(sheet.cell(rowidx, colidx + 3).value)
                        # v.append(x)
                        v = x
        return (v)        
                    
def scrape_firstnames(text_name):
    # text_name = str(text_name)
    fname = ""
    if '&' in text_name:
        text_name_split = text_name.split("&")
        text_name_len = len(text_name_split) - 2
        i = 0
        for text_names in text_name_split:
            # pass
            f = text_names.strip().split(" ")
            if (i < text_name_len):
                separator = ", "
            else:
                separator = " & "
            i = i + 1
            #fname+=f[0] + " & "
            fname+=f[0] + separator
        return (str(fname.rstrip(' & ')))
    else:
        text_name_split = text_name.split(" ")
        return text_name_split[0]

# search_str = "1/25 Princess St\nBulimba QLD 4171" 1/25 Princess St Bulimba QLD 4171

# search_str = input("Please enter address: ")    
# while not search_str:
#     search_str = input("Please enter address: ")

#------------------------------------------------------------------------------
# WELCOME LETTER EXECUTION
#------------------------------------------------------------------------------
def exec_welcome():
    global defaultEmailRecipient
    global client_email_address

    global serviceGuaranteeTemplatePath 
    global serviceGuaranteeTemplateFilename 
    global serviceGuaranteeNewPath 
    global serviceGuaranteeNewFilename 
    global serviceGuaranteePdfPath 
    global serviceGuaranteePdfFilename 

    global welcomeDocTemplatePath 
    global welcomeDocTemplateFilename 
    global welcomeDocNewPath 
    global welcomeDocNewFilename 
    global welcomeDocPdfPath 
    global welcomeDocPdfFilename 

    global welcomeLetterEmailTemplatePath 
    global welcomeLetterEmailTemplateFilename 
    global welcomeLetterEmailNewPath 
    global welcomeLetterEmailNewFilename 

    user_name = os.getlogin().lower()
    # defaultEmailRecipient = os.getlogin().lower()
    # if 'joefer' in user_name:
    #     defaultEmailRecipient = 'joefer333@gmail.com'
    # else:
    #     defaultEmailRecipient = 'lauren@rentalresults.com.au'
        # defaultEmailRecipient = 'mark.ramos@affordablestaff.com.au'

    data_file = open('PropertyMe.csv')
    data = csv.reader(data_file)

    for row in data:
        if len(row) == 4:
            address = row[0]
            first_name = row[1]
            last_name = row[2]
            client_email_address = row[3]
        else:
            print('Invalid data found in csv file')
            input('Press enter to exit...')
            sys.exit()

    if not 'address' in locals():
        print('No data found in csv file')
        input('Press enter to exit...')
        sys.exit()

    address_unit_and_streetnumber_orig = address.split(' ')[0].strip()
    address_unit_and_streetnumber_hyphened = address.split(' ')[0].replace('/','-').strip()
    address_streetname = address.split(' ',1)[1].split(',')[0].strip()

    date_today_service_guarantee = datetime.today().strftime('%d/%m/%Y')
    date_today_welcome_doc = custom_strftime('{S} of %B %Y', datetime.now())

    cur_dir = os.getcwd()
    serviceGuaranteeTemplatePath = os.path.join(cur_dir, 'resources')
    serviceGuaranteeTemplateFilename = 'Service Guarantee_Template.docx'
    serviceGuaranteeNewPath = os.path.join(cur_dir, 'resources')
    serviceGuaranteeNewFilename = 'Service Guarantee.docx'
    serviceGuaranteePdfPath = os.path.join(cur_dir, 'PROPERTY\\' + address_streetname + ' ' + address_unit_and_streetnumber_hyphened + '\\OWNER')
    serviceGuaranteePdfFilename = 'Service Guarantee.pdf'

    welcomeDocTemplatePath = os.path.join(cur_dir, 'resources')
    welcomeDocTemplateFilename = 'Welcome_Template.docx'
    welcomeDocNewPath = os.path.join(cur_dir, 'resources')
    welcomeDocNewFilename = 'Welcome.docx'
    welcomeDocPdfPath = os.path.join(cur_dir, 'PROPERTY\\{street}\\OWNER').replace('{street}',address_streetname + ' ' + address_unit_and_streetnumber_hyphened)
    welcomeDocPdfFilename = 'Welcome.pdf'

    welcomeLetterEmailTemplatePath = os.path.join(cur_dir, 'resources')
    welcomeLetterEmailTemplateFilename = 'Welcome Letter Email Template.docx'
    welcomeLetterEmailNewPath = os.path.join(cur_dir, 'resources')
    welcomeLetterEmailNewFilename = 'Welcome Letter Email New.docx'

    if not os.path.exists(welcomeDocPdfPath):
        os.makedirs(welcomeDocPdfPath)
    elif os.path.exists(welcomeDocPdfPath):
        winsound.Beep(500, 500)
        continueInput = 0
        while not continueInput:
            try:
                print()
                print('*' * 100)
                print('It appears that a folder for ' + address_streetname + ' ' + address_unit_and_streetnumber_hyphened +
                       ' already exists under this path:\n' + welcomeDocPdfPath + '\n')
                print('Proceeding with this automation will result in saving over the PDF files inside the said folder.')
                print('\tYes - Replace the older files\n'
                      '\tNo - Abort the app / Quit')
                yes = {'yes', 'y', 'ye', ''}
                no = {'no', 'n'}
                time.sleep(2)
                print()
                continueInput = input('Continue? [y/n]: ').strip()
                print('*' * 100)
                print()
                if continueInput not in yes and continueInput not in no:
                    raise ValueError
                if continueInput in yes:
                    pass
                if continueInput in no:
                    sys.exit()
            except ValueError:
                continueInput = 0
                print('Hey, that is not an option!')
                winsound.PlaySound("SystemHand", winsound.SND_ALIAS)        

    #--------------words to replace in Service Guarantee document------------------
    word1 = re.compile(r'\bFIRSTNAME\b')
    replace1 = first_name
    word2 = re.compile(r'\bLASTNAME\b')
    replace2 = last_name
    word3 = re.compile(r'\bADDRESS\b')
    replace3 = address
    word4 = re.compile(r'\bDATE\b')
    replace4 = date_today_service_guarantee

    #--------------words to replace in Welcome document------------------
    word5 = re.compile(r'\bDATE\b')
    replace5 = date_today_welcome_doc
    word6 = re.compile(r'\bFIRSTNAME\b')
    replace6 = first_name
    replace6 = replace6.replace(";", "&")
    replace6 = scrape_firstnames(replace6)
    word7 = re.compile(r'\bADDRESS\b')
    replace7 = address

    #--------------words to replace in Welcome Letter Email document------------------
    word8 = re.compile(r'\bFIRSTNAME\b')
    replace8 = first_name
    replace8 = replace8.replace(";", "&")
    replace8 = scrape_firstnames(replace8)

    doc1 = Document(os.path.join(serviceGuaranteeTemplatePath, serviceGuaranteeTemplateFilename))
    docx_replace_word(doc1, word1 , replace1)
    docx_replace_word(doc1, word2 , replace2)
    docx_replace_word(doc1, word3 , replace3)
    docx_replace_word(doc1, word4 , replace4)
    print('Saving Service Guarantee.Docx')
    doc1.save(os.path.join(serviceGuaranteeNewPath, serviceGuaranteeNewFilename))

    doc2 = Document(os.path.join(welcomeDocTemplatePath, welcomeDocTemplateFilename))
    docx_replace_word(doc2, word5 , replace5)
    docx_replace_word(doc2, word6 , replace6)
    docx_replace_word(doc2, word7 , replace7)
    print('Saving Welcome.docx')
    doc2.save(os.path.join(welcomeDocNewPath, welcomeDocNewFilename))

    doc3 = Document(os.path.join(welcomeLetterEmailTemplatePath, welcomeLetterEmailTemplateFilename))
    docx_replace_word(doc3, word8 , replace8)
    print('Saving Welcome Letter Email New.docx (A modified version that gets copied into the email body, and deleted right after)')
    doc3.save(os.path.join(welcomeLetterEmailNewPath, welcomeLetterEmailNewFilename))

    if os.path.exists(os.path.join(serviceGuaranteePdfPath, serviceGuaranteePdfFilename)):
        os.remove(os.path.join(serviceGuaranteePdfPath, serviceGuaranteePdfFilename))
    print('Saving PDF copy of Service Guarantee doc')
    covx_to_pdf(os.path.join(serviceGuaranteeNewPath, serviceGuaranteeNewFilename), os.path.join(serviceGuaranteePdfPath, serviceGuaranteePdfFilename))
    print('Deleting Service Guarantee.docx')
    os.remove(os.path.join(serviceGuaranteeNewPath, serviceGuaranteeNewFilename))

    if os.path.exists(os.path.join(welcomeDocPdfPath, welcomeDocPdfFilename)):
        os.remove(os.path.join(welcomeDocPdfPath, welcomeDocPdfFilename))
    print('Saving PDF copy of Welcome doc')
    covx_to_pdf(os.path.join(welcomeDocNewPath, welcomeDocNewFilename), os.path.join(welcomeDocPdfPath, welcomeDocPdfFilename))
    print('Deleting Welcome_Document.docx')
    os.remove(os.path.join(welcomeDocNewPath, welcomeDocNewFilename))

    if not outlook_is_running():
        os.startfile("outlook")

    #----------------------------OUTLOOK------------------------------------------#
    subject = address_unit_and_streetnumber_orig + ' ' + address_streetname + ' - Welcome Letter'
    recipient = defaultEmailRecipient
    emailer(subject, recipient)    
    print('Done! You can find the generated PDFs inside the \'PROPERTY\' folder:\n\t' + welcomeDocPdfPath)
    winsound.PlaySound("SystemExit", winsound.SND_ALIAS)




#------------------------------------------------------------------------------
# VACATE LETTER EXECUTION
#------------------------------------------------------------------------------
def exec_vacate():
    global defaultEmailRecipient
    global client_email_address

    global vacateFORM14aRTAPath
    global vacateFORM14aRTAFilename

    global vacateRentalResultsChecklistPath
    global vacateRentalResultsChecklistFilename

    global vacateRTAMovingOutBookletPath
    global vacateRTAMovingOutBookletFilename

    global vacateLetterTemplatePath
    global vacateLetterTemplateFilename
    global vacateLetterNEWTemplatePath
    global vacateLetterNEWTemplateFilename
    global vacateLetterPDFPath
    global vacateLetterPDFFilename

    global vacateEmailTemplatePath
    global vacateEmailTemplateFilename
    global vacateEmailNEWTemplatePath
    global vacateEmailNEWTemplateFilename
    global tenant_email
    global orig_address
    global address_unit_and_streetnumber_orig

    user_name = os.getlogin().lower()
    # defaultEmailRecipient = os.getlogin().lower()
    # if 'joefer' in user_name:
    #     defaultEmailRecipient = 'joefer333@gmail.com'
    # else:
    #     defaultEmailRecipient = 'lauren@rentalresults.com.au'
        # defaultEmailRecipient = 'mark.ramos@affordablestaff.com.au'

    data_file = open('PropertyMeVacate.csv')
    data = csv.reader(data_file)

    for row in data:
        if len(row) == 12:
            address = row[0]
            first_name = row[1]
            last_name = row[2]
            client_email_address = row[3]
            tenant_firstname = row[4]
            tenant_lastname = row[5]
            tenant_moveout_date = row[6]
            tenant_email = row[7]
        else:
            print('Invalid data found in csv file')
            input('Press enter to exit...')
            sys.exit()

    if not 'address' in locals():
        print('No data found in csv file')
        input('Press enter to exit...')
        sys.exit()

    orig_address = address
    address_unit_and_streetnumber_orig = address.split(' ')[0].strip()
    address_unit_and_streetnumber_hyphened = address.split(' ')[0].replace('/','-').strip()
    address_streetname = address.split(' ',1)[1].split(',')[0].strip()

    # date_today_service_guarantee = datetime.today().strftime('%d/%m/%Y')
    date_today_welcome_doc = custom_strftime('{S} of %B %Y', datetime.now())

    cur_dir = os.getcwd()

    # serviceGuaranteeTemplatePath = os.path.join(cur_dir, 'resources\\vacate')
    # serviceGuaranteeTemplateFilename = 'Service Guarantee_Template.docx'
    # serviceGuaranteeNewPath = os.path.join(cur_dir, 'resources')
    # serviceGuaranteeNewFilename = 'Service Guarantee.docx'
    # serviceGuaranteePdfPath = os.path.join(cur_dir, 'PROPERTY\\' + address_streetname + ' ' + address_unit_and_streetnumber_hyphened + '\\OWNER')
    # serviceGuaranteePdfFilename = 'Service Guarantee.pdf'

    vacateLetterTemplatePath = os.path.join(cur_dir, 'resources\\vacate')
    vacateLetterTemplateFilename = 'Vacate Letter.docx'
    vacateLetterNEWTemplatePath  = os.path.join(cur_dir, 'resources\\vacate')
    vacateLetterNEWTemplateFilename = 'Vacate Letter New.docx'
    vacateLetterPDFPath = os.path.join(cur_dir, 'PROPERTY\\' + address_streetname + ' ' + address_unit_and_streetnumber_hyphened + '\\VACATE')
    vacateLetterPDFFilename = 'Vacate Letter.pdf'

    vacateFORM14aRTAPath = os.path.join(cur_dir, 'resources\\vacate')
    vacateFORM14aRTAFilename = 'FORM 14a RTA-exit-condition-report-form14a.pdf'

    vacateRentalResultsChecklistPath = os.path.join(cur_dir, 'resources\\vacate')
    vacateRentalResultsChecklistFilename = 'Rental Results cleaning checklist.pdf'

    vacateRTAMovingOutBookletPath = os.path.join(cur_dir, 'resources\\vacate')
    vacateRTAMovingOutBookletFilename = 'RTA Moving out booklet RTA.pdf'

    vacateEmailTemplatePath = os.path.join(cur_dir, 'resources')
    vacateEmailTemplateFilename = 'Vacate_Template.docx'
    vacateEmailNEWTemplatePath = os.path.join(cur_dir, 'resources')
    vacateEmailNEWTemplateFilename = 'Vacate_Template New.docx'

    if not os.path.exists(vacateLetterPDFPath):
        os.makedirs(vacateLetterPDFPath)
    elif os.path.exists(vacateLetterPDFPath):
        winsound.Beep(500, 500)
        continueInput = 0
        while not continueInput:
            try:
                print()
                print('*' * 100)
                print('It appears that a folder for ' + address_streetname + ' ' + address_unit_and_streetnumber_hyphened +
                       ' already exists under this path:\n' + vacateLetterPDFPath + '\n')
                print('Proceeding with this automation will result in saving over the PDF files inside the said folder.')
                print('\tYes - Replace the older files\n'
                      '\tNo - Abort the app / Quit')
                yes = {'yes', 'y', 'ye', ''}
                no = {'no', 'n'}
                time.sleep(2)
                print()
                continueInput = input('Continue? [y/n]: ').strip()
                print('*' * 100)
                print()
                if continueInput not in yes and continueInput not in no:
                    raise ValueError
                if continueInput in yes:
                    pass
                if continueInput in no:
                    sys.exit()
            except ValueError:
                continueInput = 0
                print('Hey, that is not an option!')
                winsound.PlaySound("SystemHand", winsound.SND_ALIAS)        

    #--------------words to replace in Service Guarantee document------------------
    word1 = re.compile(r'\bFIRSTNAME\b')
    # replace1 = tenant_firstname.replace(";", " & ")
    replace1 = tenant_firstname
    replace1 = replace1.replace(";", "&")
    replace1 = scrape_firstnames(replace1)
    word2 = re.compile(r'\bLASTNAME\b')
    replace2 = tenant_lastname
    word3 = re.compile(r'\bADDRESS\b')
    replace3 = address
    word4 = re.compile(r'\bDATE\b')
    replace4 = date_today_welcome_doc
    word9 = re.compile(r'\bMOVEOUTDATE\b')
    replace9 = tenant_moveout_date

    #--------------words to replace in Welcome document------------------
    # word5 = re.compile(r'\bDATE\b')
    # replace5 = date_today_welcome_doc
    # word6 = re.compile(r'\bFIRSTNAME\b')
    # replace6 = first_name
    # word7 = re.compile(r'\bADDRESS\b')
    # replace7 = address

    #--------------words to replace in Welcome Letter Email document------------------
    word8 = re.compile(r'\bFIRSTNAME\b')
    replace8 = tenant_firstname
    replace8 = replace8.replace(";", " & ")
    replace8 = scrape_firstnames(replace8)

    doc1 = Document(os.path.join(vacateLetterTemplatePath, vacateLetterTemplateFilename))
    docx_replace_word(doc1, word1 , replace1)
    docx_replace_word(doc1, word2 , replace2)
    docx_replace_word(doc1, word3 , replace3)
    docx_replace_word(doc1, word4 , replace4)
    docx_replace_word(doc1, word9 , replace9)
    print('Saving Vacate Letter.docx')
    doc1.save(os.path.join(vacateLetterNEWTemplatePath, vacateLetterNEWTemplateFilename))

    # doc2 = Document(os.path.join(welcomeDocTemplatePath, welcomeDocTemplateFilename))
    # docx_replace_word(doc2, word5 , replace5)
    # docx_replace_word(doc2, word6 , replace6)
    # docx_replace_word(doc2, word7 , replace7)
    # print('Saving Welcome.docx')
    # doc2.save(os.path.join(welcomeDocNewPath, welcomeDocNewFilename))

    doc3 = Document(os.path.join(vacateEmailTemplatePath, vacateEmailTemplateFilename))
    docx_replace_word(doc3, word8 , replace8)
    print('Saving Vacate_Template New.docx (A modified version that gets copied into the email body, and deleted right after)')
    doc3.save(os.path.join(vacateEmailNEWTemplatePath, vacateEmailNEWTemplateFilename))

    if os.path.exists(os.path.join(vacateLetterPDFPath, vacateLetterPDFFilename)):
        os.remove(os.path.join(vacateLetterPDFPath, vacateLetterPDFFilename))
    print('Saving PDF copy of Vacate Letter doc')
    covx_to_pdf(os.path.join(vacateLetterNEWTemplatePath , vacateLetterNEWTemplateFilename), os.path.join(vacateLetterPDFPath, vacateLetterPDFFilename))
    print('Deleting Vacate Letter.docx')
    os.remove(os.path.join(vacateLetterNEWTemplatePath, vacateLetterNEWTemplateFilename))

    # if os.path.exists(os.path.join(welcomeDocPdfPath, welcomeDocPdfFilename)):
    #     os.remove(os.path.join(welcomeDocPdfPath, welcomeDocPdfFilename))
    # print('Saving PDF copy of Welcome doc')
    # covx_to_pdf(os.path.join(welcomeDocNewPath, welcomeDocNewFilename), os.path.join(welcomeDocPdfPath, welcomeDocPdfFilename))
    # print('Deleting Welcome_Document.docx')
    # os.remove(os.path.join(welcomeDocNewPath, welcomeDocNewFilename))

    if not outlook_is_running():
        os.startfile("outlook")

    #----------------------------OUTLOOK------------------------------------------#
    subject = 'Vacate - ' + address_unit_and_streetnumber_orig + ' ' + address_streetname  #will be overwritten
    subject = 'Vacate - ' + address
    recipient = defaultEmailRecipient
    vacate_emailer(subject, recipient)    
    print('Done! You can find the generated PDFs inside the \'PROPERTY\' folder:\n\t' + vacateLetterPDFPath)
    winsound.PlaySound("SystemExit", winsound.SND_ALIAS)


#------------------------------------------------------------------------------
# BREAK LEASE EXECUTION
#------------------------------------------------------------------------------
def exec_breaklease():
    global defaultEmailRecipient
    global client_email_address

    global breakLeaseFORM14aRTAPath
    global breakLeaseFORM14aRTAFilename

    global breakLeaseRentalResultsChecklistPath
    global breakLeaseRentalResultsChecklistFilename

    global breakLeaseRTAMovingOutBookletPath
    global breakLeaseRTAMovingOutBookletFilename

    global breakLeaseAgreementTemplatePath
    global breakLeaseAgreementTemplateFilename
    global breakLeaseAgreementNEWTemplatePath
    global breakLeaseAgreementNEWTemplateFilename
    global breakLeaseAgreementPDFPath
    global breakLeaseAgreementPDFFilename

    global breakLeaseEmailTemplatePath
    global breakLeaseEmailTemplateFilename
    global breakLeaseEmailNEWTemplatePath
    global breakLeaseEmailNEWTemplateFilename
    global tenant_email
    global orig_address

    user_name = os.getlogin().lower()
    # defaultEmailRecipient = username
    # if 'joefer' in user_name:
    #     defaultEmailRecipient = 'joefer333@gmail.com'
    # else:
    #     defaultEmailRecipient = 'lauren@rentalresults.com.au'
        # defaultEmailRecipient = 'mark.ramos@affordablestaff.com.au'

    data_file = open('PropertyMeBreakLease.csv')
    data = csv.reader(data_file)

    for row in data:
        if len(row) > 0:
            address = row[0]
            first_name = row[1]
            last_name = row[2]
            client_email_address = row[3]
            tenant_firstname = row[4]
            tenant_lastname = row[5]
            tenant_moveout_date = row[6]
            tenant_email = row[7]
            tenants = row[8]
            lb_fee = row[9]
            rent = row[10]
            adfee = row[11]
        else:
            print('Invalid data found in csv file')
            input('Press enter to exit...')
            sys.exit()

    if not 'address' in locals():
        print('No data found in csv file')
        input('Press enter to exit...')
        sys.exit()

    orig_address = address
    address_unit_and_streetnumber_orig = address.split(' ')[0].strip()
    address_unit_and_streetnumber_hyphened = address.split(' ')[0].replace('/','-').strip()
    address_streetname = address.split(' ',1)[1].split(',')[0].strip()

    # date_today_service_guarantee = datetime.today().strftime('%d/%m/%Y')
    # date_today_welcome_doc = custom_strftime('{S} of %B %Y', datetime.now())

    cur_dir = os.getcwd()

    # serviceGuaranteeTemplatePath = os.path.join(cur_dir, 'resources\\vacate')
    # serviceGuaranteeTemplateFilename = 'Service Guarantee_Template.docx'
    # serviceGuaranteeNewPath = os.path.join(cur_dir, 'resources')
    # serviceGuaranteeNewFilename = 'Service Guarantee.docx'
    # serviceGuaranteePdfPath = os.path.join(cur_dir, 'PROPERTY\\' + address_streetname + ' ' + address_unit_and_streetnumber_hyphened + '\\OWNER')
    # serviceGuaranteePdfFilename = 'Service Guarantee.pdf'

    breakLeaseAgreementTemplatePath = os.path.join(cur_dir, 'resources\\break_lease')
    breakLeaseAgreementTemplateFilename = 'Break Lease Agreement.docx'
    breakLeaseAgreementNEWTemplatePath  = os.path.join(cur_dir, 'resources\\break_lease')
    breakLeaseAgreementNEWTemplateFilename = 'Break Lease Agreement New.docx'
    breakLeaseAgreementPDFPath = os.path.join(cur_dir, 'PROPERTY\\' + address_streetname + ' ' + address_unit_and_streetnumber_hyphened + '\\BREAKLEASE')
    breakLeaseAgreementPDFFilename = 'Break Lease Agreement.pdf'

    breakLeaseFORM14aRTAPath = os.path.join(cur_dir, 'resources\\break_lease')
    breakLeaseFORM14aRTAFilename = 'FORM 14a RTA-exit-condition-report-form14a.pdf'

    breakLeaseRentalResultsChecklistPath = os.path.join(cur_dir, 'resources\\break_lease')
    breakLeaseRentalResultsChecklistFilename = 'Rental Results cleaning checklist.pdf'

    breakLeaseRTAMovingOutBookletPath = os.path.join(cur_dir, 'resources\\break_lease')
    breakLeaseRTAMovingOutBookletFilename = 'RTA Moving out booklet RTA.pdf'

    breakLeaseEmailTemplatePath = os.path.join(cur_dir, 'resources')
    breakLeaseEmailTemplateFilename = 'BreakLease_Template.docx'
    breakLeaseEmailNEWTemplatePath = os.path.join(cur_dir, 'resources')
    breakLeaseEmailNEWTemplateFilename = 'BreakLease_Template New.docx'

    if not os.path.exists(breakLeaseAgreementPDFPath):
        os.makedirs(breakLeaseAgreementPDFPath)
    elif os.path.exists(breakLeaseAgreementPDFPath):
        winsound.Beep(500, 500)
        continueInput = 0
        while not continueInput:
            try:
                print()
                print('*' * 100)
                print('It appears that a folder for ' + address_streetname + ' ' + address_unit_and_streetnumber_hyphened +
                       ' already exists under this path:\n' + breakLeaseAgreementPDFPath + '\n')
                print('Proceeding with this automation will result in saving over the PDF files inside the said folder.')
                print('\tYes - Replace the older files\n'
                      '\tNo - Abort the app / Quit')
                yes = {'yes', 'y', 'ye', ''}
                no = {'no', 'n'}
                time.sleep(2)
                print()
                continueInput = input('Continue? [y/n]: ').strip()
                print('*' * 100)
                print()
                if continueInput not in yes and continueInput not in no:
                    raise ValueError
                if continueInput in yes:
                    pass
                if continueInput in no:
                    sys.exit()
            except ValueError:
                continueInput = 0
                print('Hey, that is not an option!')
                winsound.PlaySound("SystemHand", winsound.SND_ALIAS)        

    #--------------words to replace in Service Guarantee document------------------
    word1 = re.compile(r'\bTENANT1\b')
    if '&' in tenants:
        t = tenants.replace(";", "&")
        ts = t.split('&')
        ts_len = len(ts)
        if ts_len == 2:
            replace1 = ts[0].strip()
            word7 = re.compile(r'\bTENANT2\b')
            replace7 = ts[1].strip()
            word8 = re.compile(r'\bTENANT3\b')
            replace8 = ''
            word9 = re.compile(r'\bTENANT4\b')
            replace9 = ''
            word10 = re.compile(r'\bTENANT5\b')
            replace10 = ''
        elif ts_len == 3:
            replace1 = ts[0].strip()
            word7 = re.compile(r'\bTENANT2\b')
            replace7 = ts[1].strip()
            word8 = re.compile(r'\bTENANT3\b')
            replace8 = ts[2].strip()
            word9 = re.compile(r'\bTENANT4\b')
            replace9 = ''
            word10 = re.compile(r'\bTENANT5\b')
            replace10 = ''
        elif ts_len == 4:
            replace1 = ts[0].strip()
            word7 = re.compile(r'\bTENANT2\b')
            replace7 = ts[1].strip()
            word8 = re.compile(r'\bTENANT3\b')
            replace8 = ts[2].strip()
            word9 = re.compile(r'\bTENANT4\b')
            replace9 = ts[3].strip()
            word10 = re.compile(r'\bTENANT5\b')
            replace10 = ''
        elif ts_len == 5:
            replace1 = ts[0].strip()
            word7 = re.compile(r'\bTENANT2\b')
            replace7 = ts[1].strip()
            word8 = re.compile(r'\bTENANT3\b')
            replace8 = ts[2].strip()
            word9 = re.compile(r'\bTENANT4\b')
            replace9 = ts[3].strip()
            word10 = re.compile(r'\bTENANT5\b')
            replace10 = ts[4].strip()
        else:
            replace1 = tenants
            word7 = re.compile(r'\bTENANT2\b')
            replace7 = ''
            word8 = re.compile(r'\bTENANT3\b')
            replace8 = ''
            word9 = re.compile(r'\bTENANT4\b')
            replace9 = ''
            word10 = re.compile(r'\bTENANT5\b')
            replace10 = ''
    else:
        replace1 = tenants
        word7 = re.compile(r'\bTENANT2\b')
        replace7 = ''
        word8 = re.compile(r'\bTENANT3\b')
        replace8 = ''
        word9 = re.compile(r'\bTENANT4\b')
        replace9 = ''
        word10 = re.compile(r'\bTENANT5\b')
        replace10 = ''
    
    
    word11 = re.compile(r'\bADDRESS\b')
    replace11 = address
    word12 = re.compile(r'\bMOVEOUTDATE\b')
    replace12 = tenant_moveout_date
    
    word2 = re.compile(r'\bLBFEE\b')
    replace2 = lb_fee
    word3 = re.compile(r'\bADFEE\b')
    replace3 = adfee

    #--------------words to replace in Welcome document------------------
    # word5 = re.compile(r'\bDATE\b')
    # replace5 = date_today_welcome_doc
    # word6 = re.compile(r'\bFIRSTNAME\b')
    # replace6 = first_name
    # word7 = re.compile(r'\bADDRESS\b')
    # replace7 = address

    #--------------words to replace in Welcome Letter Email document------------------
    word4 = re.compile(r'\bTENANTS\b')
    replace4 = tenants.replace(";", "&")
    replace4 = scrape_firstnames(replace4)


    word5 = re.compile(r'\bLBFEE\b')
    replace5 = lb_fee
    word6 = re.compile(r'\bADFEE\b')
    adfee = str(get_adsfee_by_address(address_unit_and_streetnumber_orig))
    print('AdFee:' + adfee)
    if adfee == '':
        adfee = 'None'
    replace6 = adfee

    doc1 = Document(os.path.join(breakLeaseAgreementTemplatePath, breakLeaseAgreementTemplateFilename))
    docx_replace_word(doc1, word1 , replace1)
    docx_replace_word(doc1, word2 , replace2)
    docx_replace_word(doc1, word3 , replace3)
    docx_replace_word(doc1, word7 , replace7)
    docx_replace_word(doc1, word8 , replace8)
    docx_replace_word(doc1, word9 , replace9)
    docx_replace_word(doc1, word10 , replace10)
    docx_replace_word(doc1, word11 , replace11)
    docx_replace_word(doc1, word12 , replace12)
    print('Saving Break Lease Agreement.docx')
    doc1.save(os.path.join( breakLeaseAgreementNEWTemplatePath, breakLeaseAgreementNEWTemplateFilename))

    doc3 = Document(os.path.join( breakLeaseEmailTemplatePath, breakLeaseEmailTemplateFilename))
    docx_replace_word(doc3, word4 , replace4)
    docx_replace_word(doc3, word5 , replace5)
    docx_replace_word(doc3, word6 , replace6)
    print('Saving BreakLease_Template New.docx (A modified version that gets copied into the email body, and deleted right after)')
    doc3.save(os.path.join(breakLeaseEmailNEWTemplatePath, breakLeaseEmailNEWTemplateFilename))

    if os.path.exists(os.path.join(breakLeaseAgreementPDFPath, breakLeaseAgreementPDFFilename)):
        os.remove(os.path.join(breakLeaseAgreementPDFPath, breakLeaseAgreementPDFFilename))
    print('Saving PDF copy of Break Lease Letter doc')
    covx_to_pdf(os.path.join(breakLeaseAgreementNEWTemplatePath, breakLeaseAgreementNEWTemplateFilename), os.path.join(breakLeaseAgreementPDFPath, breakLeaseAgreementPDFFilename))
    print('Deleting Break Lease Agreement.docx')
    os.remove(os.path.join(breakLeaseAgreementNEWTemplatePath, breakLeaseAgreementNEWTemplateFilename))

    # if os.path.exists(os.path.join(welcomeDocPdfPath, welcomeDocPdfFilename)):
    #     os.remove(os.path.join(welcomeDocPdfPath, welcomeDocPdfFilename))
    # print('Saving PDF copy of Welcome doc')
    # covx_to_pdf(os.path.join(welcomeDocNewPath, welcomeDocNewFilename), os.path.join(welcomeDocPdfPath, welcomeDocPdfFilename))
    # print('Deleting Welcome_Document.docx')
    # os.remove(os.path.join(welcomeDocNewPath, welcomeDocNewFilename))

    if not outlook_is_running():
        os.startfile("outlook")

    print (defaultEmailRecipient)
    #----------------------------OUTLOOK------------------------------------------#
    subject = 'Break Lease - ' + address_unit_and_streetnumber_orig + ' ' + address_streetname 
    subject = 'Break Lease - ' + address
    recipient = defaultEmailRecipient
    breaklease_emailer(subject, recipient)    
    print('Done! You can find the generated PDFs inside the \'PROPERTY\' folder:\n\t' +  breakLeaseAgreementPDFPath)
    winsound.PlaySound("SystemExit", winsound.SND_ALIAS)


# -----Script starts----------------------------------------------------------
try:
    global keyword
    selected = extract_data()
    #print('Selected: ' + selected)
    defaultEmailRecipient = selected[1]
    keyword = selected[2]
    if is_debug == 'on': defaultEmailRecipient = 'mark.ramos@affordablestaff.com.au'

    # selection if else
    if selected[0] == '1':
        exec_welcome()
    elif selected[0] == '2':
        exec_vacate()
    elif selected[0] == '3':
        exec_breaklease()
    else: 
        exec_welcome()

    
except Exception as globalEx:
    print(globalEx)
input('Press any key to quit...')
os.system('color 0f') # sets the background to blue
sys.exit()
