# def scrape_firstnames(text_name):
#     fname = ""
#     if '&' in text_name:
#         text_name_split = text_name.split("&")
#         # text_name_len = len(text_name_split)
#         for text_names in text_name_split:
#             # pass
#             f = text_names.strip().split(" ")
#             fname+=f[0] + " & "
#         return (fname.rstrip(' & '))

def scrape_firstnames(text_name):
    # text_name = str(text_name)
    fname = ""
    if '&' in text_name:
        text_name_split = text_name.split("&")
        text_name_len = len(text_name_split) - 2
        i = 0
        for text_names in text_name_split:
            # pass
            f = text_names.strip().split(" ")
            if (i < text_name_len):
                separator = ", "
            else:
                separator = " & "
            i = i + 1
            #fname+=f[0] + " & "
            fname+=f[0] + separator
        return (str(fname.rstrip(' & ')))
    else:
        text_name_split = text_name.split(" ")
        return text_name_split[0]

x = scrape_firstnames("Mark Ramos & John Doe & jane Smith & Romeo & Juliet")
print (x)